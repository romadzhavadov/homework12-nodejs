const { useBase } = require('./base');
const { options } = require('./claOptions');

const insert = async () => {
  try {
    const { base } = await useBase();

    await base('videos').insert({
      title: options.title,
      views: options.views,
      category: options.category
    })

    console.log('insert success')
  } catch (err) {
    console.log(err)
  }
}

(async () => {
  await insert()
})()