const commandLineArgs = require('command-line-args');

const optionDefinitions = [
  { name: 'title', type: String },
  { name: 'category', type: String },
  { name: 'views', type: Number },
  { name: 'id', type: Number },
];

const options = commandLineArgs(optionDefinitions);

module.exports = {
  options
};