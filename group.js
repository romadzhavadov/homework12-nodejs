const { useBase } = require('./base');

const group = async () => {
  try {
    const { base } = await useBase();

    const result = await base('videos')
      .select('category')
      .count('views as viewsCount')
      .groupBy('category');

    console.log(result
      )

    result.forEach(row => {
      console.log(`${row.category} - ${row.viewsCount}`);
    });
  } catch (error) {
    console.error('Error:', error);
  }
};


(async () => {
  await group()
})()